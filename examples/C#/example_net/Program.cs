﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

/* A simple example to create a matrix , execute a command and get the result */


namespace example_net
{
    class Program
    {
        static void Main(string[] args)
        {

            SLApp.CoCOMServer objScilabApp;

            objScilabApp = new SLApp.CoCOMServer();
            objScilabApp.Visible = true;

  
            System.Array pr = new double[2, 4];
            pr.SetValue(11, 0, 0);
            pr.SetValue(12, 0, 1);
            pr.SetValue(13, 0, 2);
            pr.SetValue(14, 0, 3);

            pr.SetValue(1, 1, 0);
            pr.SetValue(2, 1, 1);
            pr.SetValue(3, 1, 2);
            pr.SetValue(4, 1, 3);
  

/*
            System.Array pr = new double[4];
            pr.SetValue(11, 0);
            pr.SetValue(12, 1);
            pr.SetValue(13, 2);
            pr.SetValue(14, 3);
*/

            objScilabApp.createMatrixOfDouble("A", pr);

            objScilabApp.Execute("A = A + 2;");

            System.Array prresult = objScilabApp.getMatrixOfDouble("A");



            for (int i = 1; i <= prresult.GetLength(0); i++)
            {
                for (int j = 1; j <= prresult.GetLength(1); j++)
                {
                    System.Console.Write(prresult.GetValue(i, j));
                    System.Console.Write(" ");
                }
                System.Console.WriteLine("");
            }

            System.Console.WriteLine("End");
            objScilabApp.Quit();
        }
    }
}
