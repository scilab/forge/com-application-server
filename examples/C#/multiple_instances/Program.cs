﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace multiple_instances
{
    class Program
    {
        static SLApp.CoCOMServer[] objScilabApps;

        static void Main(string[] args)
        {
            System.Console.WriteLine("Begin Main Thread");

            int nbScilab = 5; // We create 5 instances of Scilab
            objScilabApps = new SLApp.CoCOMServer[nbScilab];

            for (int i = 0; i < nbScilab; i++)
            {
                objScilabApps[i] = new SLApp.CoCOMServer();
 
                // comment if you want to have hide scilab console
                objScilabApps[i].Visible = true;
            }

            for (int i = 0; i < nbScilab; i++)
            {
                System.Console.WriteLine("Scilab #" + i + ":");
                string commandForScilab = "A" + i + "=rand(2,2);[U,S,V] = svd(A" + i + ");";
                System.Console.WriteLine(commandForScilab);
                // command send to scilab not blocking
                objScilabApps[i].putCommand(commandForScilab);
                // see Execute if you want to execute and wait result 
            }

            // You do something here ... (Your program)


            // here we wait results 
            bool doLoop = true;

            while (doLoop)
            {
                for (int i = 0; i < nbScilab; i++)
                {
                    if (objScilabApps[i].isNamedVarExist("U"))
                    {
                        if (i == nbScilab - 1)
                        {
                            doLoop = false;
                        }
                    }
                    // only to not consume all cpu activity
                    System.Threading.Thread.Sleep(1);
                }
            }


            // We read data from scilabs
            for (int i = 0; i < nbScilab; i++)
            {
                System.Console.WriteLine("Get result of U from Scilab #" + i + ":");
                System.Array prresult = objScilabApps[i].getMatrixOfDouble("U");
                for (int k = 1; k <= prresult.GetLength(0); k++)
                {
                    for (int j = 1; j <= prresult.GetLength(1); j++)
                    {
                        System.Console.Write(prresult.GetValue(k, j));
                        System.Console.Write(" ");
                    }
                    System.Console.WriteLine("");
                }
            }

            // We close all instances of Scilab
            for (int i = 0; i < nbScilab; i++)
            {
                objScilabApps[i].Quit();
            }

            System.Console.WriteLine("End Main Thread");
        }
    }
}
