//=============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//=============================================================================
#include "stdAfx.h"
//=============================================================================
bool fileExists(std::wstring filename)
{
    WIN32_FIND_DATAW FindFileData;
    HANDLE handle = FindFirstFileW (filename.c_str(), &FindFileData);
    if (handle != INVALID_HANDLE_VALUE)
    {
        FindClose (handle);
        return true;
    }
    return false;
}
//=============================================================================