//=============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//=============================================================================
#ifndef __GETSCILABPATH_HXX__
#define __GETSCILABPATH_HXX__

#include <string>

std::wstring getScilabPath(void);

#endif /* __GETSCILABPATH_HXX__ */
//=============================================================================
