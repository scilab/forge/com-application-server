//=============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//=============================================================================
#include "StdAfx.h"
//=============================================================================
char *wide_string_to_UTF8(const wchar_t *_wide)
{
	char *buf = NULL;
	DWORD size = 0;
	DWORD dwFlags = 0;

	if ((wchar_t *)NULL == _wide) return (char *)NULL;
	size = WideCharToMultiByte(CP_UTF8, dwFlags, _wide, -1, NULL, 0, NULL, 0);
	if (size == 0) return (char *)NULL;
	size += 1;
    buf = new char[size];
	if (buf)
	{
		WideCharToMultiByte(CP_UTF8, dwFlags, _wide, -1, buf, size, NULL, 0);
		if (size <= 0)
		{
			delete buf;
			return (char *)NULL;
		}
	}
	return buf;
}
//=============================================================================
wchar_t *to_wide_string(const char *_UTFStr)
{
	int nwide = 0;
	wchar_t *_buf = NULL;

	/* About MultiByteToWideChar : 
	Starting with Windows Vista, 
	the function does not drop illegal code points 
	if the application does not set this flag. 

	Windows XP: To prevent the security problem of the non-shortest-form
	versions of UTF-8 characters, MultiByteToWideChar deletes these characters.
	*/

	DWORD dwFlags = 0;

	if(_UTFStr == NULL) return NULL;
	nwide = MultiByteToWideChar(CP_UTF8, dwFlags, _UTFStr, -1, NULL, 0);
	if(nwide == 0) return NULL;
    _buf = new wchar_t[nwide];
	if(_buf == NULL) return NULL;
	if(MultiByteToWideChar(CP_UTF8, dwFlags, _UTFStr, -1, _buf, nwide) == 0)
	{
		delete _buf;
		_buf = NULL;
	}
	return _buf;
}
//=============================================================================
std::string wstringToString(std::wstring _wstr)
{
	std::string result("");
	char* converted = wide_string_to_UTF8(_wstr.c_str());
	if (converted)
	{
		result.assign(converted);
		delete converted;
		converted = NULL;
	}
	return result;
}
//=============================================================================
std::wstring stringToWstring(std::string _str)
{
	std::wstring result(L"");
	wchar_t* converted = to_wide_string(_str.c_str());
	if (converted)
	{
		result.assign(converted);
		delete converted;
		converted = NULL;
	}
	return result;
}
//=============================================================================
