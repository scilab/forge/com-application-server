//=============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//=============================================================================

#ifndef __CHARENCODING_HXX__
#define __CHARENCODING_HXX__

#include <windows.h>
#include <string>

char *wide_string_to_UTF8(const wchar_t *_wide);
wchar_t *to_wide_string(const char *_UTFStr);

std::string wstringToString(std::wstring _wstr);
std::wstring stringToWstring(std::string _str);

#endif /* __CHARENCODING_HXX__ */
//=============================================================================
