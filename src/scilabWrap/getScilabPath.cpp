//=============================================================================
// Copyright (C) 2011 - DIGITEO - Allan CORNET
//=============================================================================
#include "StdAfx.h"
//=============================================================================
const static wchar_t* gWin32SubKey = L"Software\\Scilab";
const static wchar_t* gWin64SubKey = L"Software\\Wow6432Node\\Scilab";
//=============================================================================
typedef BOOL (WINAPI *LPFN_ISWOW64PROCESS) (HANDLE, PBOOL);
static LPFN_ISWOW64PROCESS fnIsWow64Process = NULL;
static BOOL firstIsWow64Call = TRUE;
//=============================================================================
static BOOL IsWow64(void);
//=============================================================================
std::wstring getScilabPath(void)
{
    const wchar_t *subKey = NULL;
    HKEY hKeyScilab, hKeyCurrentScilab;
    DWORD dwType = 0, dwBuffSize = 0;
    std::wstring strScilabPath(L"");

    //------------------------------------------------------------------------
    //	Read registry values for last Scilab install directory.
    //  NOTE: 64-bit Windows code is currently untested.
    //
    //  We are not checking for errors in reading the registry keys.
    //  The worst that happens is that we don't have a valid directory
    //  for the Scilab path and LoadLibraryEx will later on.
    //------------------------------------------------------------------------
    if (IsWow64())		// Use Win32 or Win64 registry key
    {
        subKey = gWin64SubKey;
    }
    else
    {
        subKey = gWin32SubKey;
    }

    // Open Scilab registry key
    if (RegOpenKeyExW(HKEY_LOCAL_MACHINE, subKey, 0, KEY_READ, &hKeyScilab) ==  ERROR_SUCCESS)
    {
        wchar_t *last_install = NULL;
        wchar_t *install_dir = NULL;

        if (RegQueryValueExW(hKeyScilab, L"LASTINSTALL", NULL, &dwType, NULL, &dwBuffSize) != ERROR_SUCCESS)
        {
            return strScilabPath;
        }

        try
        {
            last_install = new wchar_t[dwBuffSize];
            if (last_install == NULL)
            {
                return strScilabPath;
            }

            // Query value of last install
            if (RegQueryValueExW(hKeyScilab, L"LASTINSTALL", NULL, &dwType, (LPBYTE)last_install, &dwBuffSize) != ERROR_SUCCESS)
            {
                if (last_install) delete [] last_install;
                return strScilabPath;
            }

            // Open last install registry key
            if (RegOpenKeyExW(hKeyScilab, last_install, 0, KEY_READ, &hKeyCurrentScilab) != ERROR_SUCCESS)
            {
                RegCloseKey(hKeyScilab);
                if (last_install) delete [] last_install;
                return strScilabPath;
            }

            // Query Scilab path length
            if (RegQueryValueExW(hKeyCurrentScilab, L"SCIPATH", NULL, &dwType, NULL, &dwBuffSize) != ERROR_SUCCESS)
            {
                RegCloseKey(hKeyScilab);
                if (last_install) delete [] last_install;
                return strScilabPath;
            }

            install_dir = new wchar_t[dwBuffSize];
            if (install_dir == NULL)
            {
                if (last_install) delete [] last_install;
                return strScilabPath;
            }

            // Query value of Scilab path
            if (RegQueryValueExW(hKeyCurrentScilab, L"SCIPATH", NULL, &dwType, (LPBYTE)install_dir, &dwBuffSize) != ERROR_SUCCESS)
            {
                RegCloseKey(hKeyScilab);
                RegCloseKey(hKeyCurrentScilab);
                if (install_dir) delete [] install_dir;
                if (last_install) delete [] last_install;
                return strScilabPath;
            }
            else
            {
                RegCloseKey(hKeyScilab);
                RegCloseKey(hKeyCurrentScilab);
                if (last_install) delete [] last_install;
                strScilabPath.assign(install_dir);
                if (install_dir) delete [] install_dir;
            }
        }
        catch (std::bad_alloc&)
        {
        }
    }
    return strScilabPath;
}
//=============================================================================
static BOOL IsWow64(void)
{
    BOOL bIsWow64 = FALSE;

    if (firstIsWow64Call)
    {
        if (fnIsWow64Process == NULL)
        {
            fnIsWow64Process = (LPFN_ISWOW64PROCESS)GetProcAddress(GetModuleHandle("kernel32"), "IsWow64Process");
        }
        firstIsWow64Call = FALSE;
    }

    if (fnIsWow64Process)
    {
        if (!fnIsWow64Process(GetCurrentProcess(), &bIsWow64)) return FALSE;
    }
    return bIsWow64;
}
//=============================================================================
