// ============================================================================
// Allan CORNET - 2011
// ============================================================================
// ComScilabServer.cpp : Defines the entry point for the application.
// ============================================================================
#include "stdafx.h"
#include <objbase.h>
#include <shellapi.h>
#include "ComScilabServer_h.h"
#include "ComScilabServerImpl.h"
// ============================================================================
bool bStandardStart = true;
// ============================================================================
DWORD WINAPI MyThreadFunction( LPVOID lpParam )
{
	int iMode = 0;
	if (bStandardStart)
	{
		iMode = 1;
	}
    int ierr = Windows_Main(NULL, NULL, NULL, iMode);
    return ierr;
}
// ============================================================================
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
    LPWSTR *szArglist = NULL;
    int nArgs = 0;
  	
    szArglist = CommandLineToArgvW(GetCommandLineW(), &nArgs);
    if(szArglist)
    {
         for(int i = 0; i < nArgs; i++) 
         {
             if (_wcsicmp(szArglist[i], L"/UNREGSERVER") == 0)
             {
                 DllUnregisterServer();
                 return 0;
             }

             if (_wcsicmp(szArglist[i], L"/REGSERVER") == 0)
             {
                 DllRegisterServer();
                 return 0;
             }

			 if (_wcsicmp(szArglist[i], L"-Embedding") == 0)
			 {
				 bStandardStart = false;
			 }
         }
    }

	if (bStandardStart)
	{

	}
	else
	{

	}

	// initialize the COM library
	::CoInitialize(NULL);

	// register ourself as a class object against the internal COM table
	// (this has nothing to do with the registry)
	DWORD nToken = CoEXEInitialize();

    loadSymbols();
    {
        DWORD   dwThreadIdArray = 0;
        HANDLE  hThreadArray = INVALID_HANDLE_VALUE;

        hThreadArray = CreateThread( 
            NULL,
            0,
            MyThreadFunction,
            NULL,
            0,
            &dwThreadIdArray);
		
		// here a ugly workaround to "wait" Scilab fully started
		// I must add a function in Scilab
		Sleep(5000);
		while(isNamedVarExist(L"stringlib") == false)
		{
			Sleep(1);
		}
    }

	// -- the message loop ----------------
	//
	// (loop ends if WM_QUIT message is received)
	//
	MSG msg;
	while (GetMessage(&msg, 0, 0, 0) > 0) 
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	// unregister from the known table of class objects
	CoEXEUninitialize(nToken);

	// 
	::CoUninitialize();

	return 0;
}
// ============================================================================
