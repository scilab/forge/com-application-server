// ============================================================================
// Allan CORNET - 2011
// ============================================================================


#pragma once

// IScilabApp interface declaration ///////////////////////////////////////////
//
//

class CoCOMServer : public IScilabApp
{

	// Construction
public:
	CoCOMServer();
	~CoCOMServer();

	// IUnknown implementation
	//
	virtual HRESULT STDMETHODCALLTYPE QueryInterface(const IID& iid, void** ppv) ;
	virtual ULONG STDMETHODCALLTYPE AddRef() ;
	virtual ULONG STDMETHODCALLTYPE Release() ;

	//IDispatch implementation
	virtual HRESULT STDMETHODCALLTYPE GetTypeInfoCount(UINT* pctinfo);
	virtual HRESULT STDMETHODCALLTYPE GetTypeInfo(UINT itinfo, LCID lcid, ITypeInfo** pptinfo);
	virtual HRESULT STDMETHODCALLTYPE GetIDsOfNames(REFIID riid, 
		LPOLESTR* rgszNames, UINT cNames,
		LCID lcid, DISPID* rgdispid);
	virtual HRESULT STDMETHODCALLTYPE Invoke(DISPID dispidMember, REFIID riid,
		LCID lcid, WORD wFlags, DISPPARAMS* pdispparams, VARIANT* pvarResult,
		EXCEPINFO* pexcepinfo, UINT* puArgErr);

	// IScilabApp implementation
	//
    virtual int STDMETHODCALLTYPE putCommand(/*in*/BSTR objectname);
    virtual int STDMETHODCALLTYPE Quit(void);
	virtual int STDMETHODCALLTYPE Execute(BSTR commandLine);

    virtual /* [id] */ HRESULT STDMETHODCALLTYPE getScalarDouble( 
        /* [in] */ BSTR varname,
        /* [retval][out] */ double *pValue);

	virtual /* [id] */ HRESULT STDMETHODCALLTYPE getScalarString( 
		/* [in] */ BSTR varname,
		/* [out] */ BSTR *strval);

    virtual /* [id] */ HRESULT STDMETHODCALLTYPE getScalarBoolean( 
        /* [in] */ BSTR varname,
        /* [retval][out] */ VARIANT_BOOL *pValue);

    virtual /* [id] */ HRESULT STDMETHODCALLTYPE createScalarDouble( 
        /* [in] */ BSTR varname,
        /* [in] */ double pValue);

    virtual /* [id] */ HRESULT STDMETHODCALLTYPE createScalarString( 
        /* [in] */ BSTR varname,
        /* [in] */ BSTR strval);

    virtual /* [id] */ HRESULT STDMETHODCALLTYPE createScalarBoolean( 
        /* [in] */ BSTR varname,
        /* [in] */ VARIANT_BOOL bValue);

	virtual /* [id] */ HRESULT STDMETHODCALLTYPE getComplexMatrixOfDouble( 
		/* [in] */ BSTR varname,
		/* [out][in] */ SAFEARRAY * *pr,
		/* [out][in] */ SAFEARRAY * *pi);

	virtual /* [id] */ HRESULT STDMETHODCALLTYPE getMatrixOfDouble( 
		/* [in] */ BSTR varname,
		/* [out][in] */ SAFEARRAY * *pr);

	virtual /* [id] */ HRESULT STDMETHODCALLTYPE createMatrixOfDouble( 
		/* [in] */ BSTR varname,
		/* [in] */ SAFEARRAY * *pr);

	virtual /* [id] */ HRESULT STDMETHODCALLTYPE isNamedVarExist( 
		/* [in] */ BSTR varname,
		/* [retval][out] */ VARIANT_BOOL *isExist);


    virtual HRESULT STDMETHODCALLTYPE get_Visible(VARIANT_BOOL* pValue);
    virtual HRESULT STDMETHODCALLTYPE put_Visible( VARIANT_BOOL newValue) ;


private:

	HRESULT LoadTypeInfo(ITypeInfo ** pptinfo, const CLSID& libid, const CLSID& iid, LCID lcid);

	// Reference count
	long		m_cRef ;
	LPTYPEINFO	m_ptinfo; // pointer to type-library
};






///////////////////////////////////////////////////////////
//
// Class factory
//
class CFactory : public IClassFactory
{
public:
	// IUnknown
	virtual HRESULT STDMETHODCALLTYPE QueryInterface(const IID& iid, void** ppv) ;         
	virtual ULONG   STDMETHODCALLTYPE AddRef() ;
	virtual ULONG   STDMETHODCALLTYPE Release() ;

	// Interface IClassFactory
	virtual HRESULT STDMETHODCALLTYPE CreateInstance(IUnknown* pUnknownOuter,
	                                         const IID& iid,
	                                         void** ppv) ;
	virtual HRESULT STDMETHODCALLTYPE LockServer(BOOL bLock) ; 

	// Constructor
	CFactory() : m_cRef(1) {}

	// Destructor
	~CFactory() {;}

private:
	long m_cRef ;
} ;



DWORD CoEXEInitialize();
void CoEXEUninitialize(DWORD nToken);
STDAPI DllRegisterServer();
STDAPI DllUnregisterServer();